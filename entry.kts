#!/usr/bin/env kscript
@file:MavenRepository("ktor-releases","https://kotlin.bintray.com/ktor" )

@file:DependsOn("io.ktor:ktor-server-jetty:1.3.1")
@file:DependsOn("io.ktor:ktor-server-core:1.3.1")

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.jetty.*

embeddedServer(Jetty, applicationEngineEnvironment {
    module { main() }
    connector {
        host = "0.0.0.0"
        port = 8888
    }
}).start(wait = true)

fun Application.main() {
    routing {
        get("/") { call.respond("hello world") }
    }
}
